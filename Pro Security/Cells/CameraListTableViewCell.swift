//
//  CameraListTableViewCell.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit

class CameraListTableViewCell: UITableViewCell {

    @IBOutlet var ipOutlet: UILabel!
    @IBOutlet var modelOutlet: UILabel!
    @IBOutlet var statusOutlet: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
