//
//  MediaPictureCollectionViewCell.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit

class MediaPictureCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    
}
