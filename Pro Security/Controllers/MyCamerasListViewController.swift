//
//  MyCamerasListViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit
import RealmSwift
import ONVIFCamera

private let reuseIdentifier = "CameraListTableViewCell"
private let segueIdentifier = "ConnectToCamera"

class MyCamerasListViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var viewCameraEmpty: UIView!
    
    var camera: ONVIFCamera = ONVIFCamera(with: "XX", credential: nil)
    var uri = ""
    
    var camerasList: Results<Camera>! = nil
    var cameraNumber = 0
    var playSegue = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Cameras"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadCameras()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if self.playSegue {
            self.playSegue = false
            let destinationVC = segue.destination as! CameraDetailViewController
            destinationVC.URI = self.camera.streamURI
        }
        
    }
    
    func loadCameras() {
        let realm = try! Realm()
        self.camerasList = realm.objects(Camera.self)
        cameraNumber = self.camerasList.count
        self.tableView.reloadData()
        if self.camerasList.count > 0 {
            self.viewCameraEmpty.isHidden = true
        } else {
            self.viewCameraEmpty.isHidden = false
        }
    }
    
    private func getDeviceInformation() {
        camera.getCameraInformation(callback: { (camera) in
            // Camera is Connected
            self.updateProfiles()
        }, error: { (reason) in
            // Camera not Connected
            let alert = UIAlertController(title: "Error", message: reason, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    private func updateProfiles() {
        if camera.state == .Connected {
            camera.getProfiles(profiles: { (profiles) -> () in
                if profiles.count > 0 {
                    // Retrieve the streamURI with the latest profile
                    self.camera.getStreamURI(with: profiles.first!.token, uri: { (uri) in
                        print("URI: \(uri)")
                        self.uri = uri
                        let alert = UIAlertController(title: "Perfect", message: "Camera is connected", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
                        alert.addAction(UIAlertAction(title: "Play", style: .default, handler: { action in
                            self.playSegue = true
                            self.performSegue(withIdentifier: segueIdentifier, sender: nil)
                        }))
                        
                        self.present(alert, animated: true, completion: nil)
                    })
                }
            })
        }
    }
    
}

extension MyCamerasListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cameraNumber
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! CameraListTableViewCell
        cell.ipOutlet.text = self.camerasList[indexPath.row].cameraName
        // cell.modelOutlet.text = self.camerasList[indexPath.row].cameraModel
        // cell.statusOutlet.text = self.camerasList[indexPath.row].cameraStatus
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(38)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.camera = ONVIFCamera(with: self.camerasList[indexPath.row].cameraIp,
                                  credential: (login: self.camerasList[indexPath.row].cameraUserName, password: self.camerasList[indexPath.row].cameraPassword),
                                  soapLicenseKey: Config.soapLicenseKey)
        self.camera.getServices {
            self.getDeviceInformation()
        }
        // performSegue(withIdentifier: segueIdentifier, sender: nil)
    }
}
