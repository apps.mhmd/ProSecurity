//
//  MediasViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 03/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import UIKit
import RealmSwift

private let PictureDetailSegue = "PictureDetailSegue"

class MediasViewController: UIViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    
    var imageSavedList: Results<ImageSaved>! = nil
    var imageSavedNumber = 0
    var goToPictureDetail = false
    var selectedPicture = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Media"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadPictures()
    }
    
    func loadPictures() {
        let realm = try! Realm()
        self.imageSavedList = realm.objects(ImageSaved.self)
        imageSavedNumber = self.imageSavedList.count
        self.collectionView.reloadData()
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if self.goToPictureDetail {
            self.goToPictureDetail = false
            let destinationNavigationController = segue.destination as! UINavigationController
            let destinationVC = destinationNavigationController.topViewController as! PictureDetailViewController
            destinationVC.imageS = UIImage(data:self.imageSavedList[selectedPicture].imageSaved as! Data,scale:1.0)
        }
     }
    
}

extension MediasViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageSavedNumber
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MediaPictureCollectionViewCell", for: indexPath) as! MediaPictureCollectionViewCell
        cell.imageView.image = UIImage(data:self.imageSavedList[indexPath.row].imageSaved as! Data,scale:1.0)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedPicture = indexPath.row
        self.goToPictureDetail = true
        performSegue(withIdentifier: PictureDetailSegue, sender: nil)
    }
}
