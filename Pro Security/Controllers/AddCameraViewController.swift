//
//  AddCameraViewController.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 20/04/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import Eureka
import RealmSwift
import ONVIFCamera

class AddCameraViewController: FormViewController {
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveAction(_ sender: Any) {
        
        let newCamera = Camera()
        
        if let name = self.form.rowBy(tag: "name") as? TextRow {
            if name.value == "" {
                alertMissingData()
                return
            }
            newCamera.cameraName = name.value!
        }
        if let ip = self.form.rowBy(tag: "ip") as? TextRow {
            if ip.value == "" {
                alertMissingData()
                return
            }
            newCamera.cameraIp = ip.value!
        }
        if let username = self.form.rowBy(tag: "username") as? TextRow {
            if username.value == "" {
                alertMissingData()
                return
            }
            newCamera.cameraUserName = username.value!
        }
        if let password = self.form.rowBy(tag: "password") as? TextRow {
            if password.value == "" {
                alertMissingData()
                return
            }
            newCamera.cameraPassword = password.value!
        }
        if let make = self.form.rowBy(tag: "make") as? TextRow {
            newCamera.cameraMake = make.value!
        }
        if let model = self.form.rowBy(tag: "model") as? TextRow {
            newCamera.cameraModel = model.value!
        }
        if let status = self.form.rowBy(tag: "status") as? TextRow {
            newCamera.cameraStatus = status.value!
        }
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(newCamera, update: false)
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Add Camera"
        
        form +++ Section("")
            <<< TextRow(){
                $0.title = ""
                $0.tag = "name"
                $0.placeholder = "Name"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            
            <<< TextRow(){
                $0.title = ""
                $0.tag = "ip"
                $0.placeholder = "xxx.xxx.xxx.xxx:yy"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            
            <<< TextRow(){
                $0.title = ""
                $0.tag = "username"
                $0.placeholder = "Username"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            
            <<< TextRow(){
                $0.title = ""
                $0.tag = "password"
                $0.placeholder = "Password"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            <<< TextRow(){
                $0.title = ""
                $0.tag = "make"
                $0.placeholder = "Camera Make(Optional)"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            <<< TextRow(){
                $0.title = ""
                $0.tag = "model"
                $0.placeholder = "Model(Optional)"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
            }
            <<< TextRow(){
                $0.title = ""
                $0.tag = "status"
                $0.placeholder = "Status(Optional)"
                $0.value = ""
                $0.add(rule: RuleRequired())
                $0.validationOptions = .validatesOnChange
        }
    }
    
    func alertMissingData() {
        let alert = UIAlertController(title: "Alert", message: "Verify Input", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
