//
//  Camera.swift
//  Pro Security
//
//  Created by Douik Mohamed Houcem on 02/06/2018.
//  Copyright © 2018 MHMDAPPS. All rights reserved.
//

import RealmSwift

class Camera: Object {
    
    @objc dynamic var cameraName = ""
    @objc dynamic var cameraMake = ""
    @objc dynamic var cameraModel = ""
    @objc dynamic var cameraIp = ""
    @objc dynamic var cameraHttpPort = ""
    @objc dynamic var cameraRtspPort = ""
    @objc dynamic var cameraSoap = true
    @objc dynamic var cameraAuth = true
    @objc dynamic var cameraUserName = ""
    @objc dynamic var cameraPassword = ""
    @objc dynamic var cameraStatus = "Camera is live"
    
    override static func primaryKey() -> String? {
        return "cameraIp"
    }
    
}
